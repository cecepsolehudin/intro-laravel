<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Buat Account Baru</h1>

	<h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf

        <p>First Name:</p>
		<input type="text" name="firstname">
		<p>Last Name:</p>
		<input type="text" name="lastname">
		<br><br>

		<label>Gender:</label>
		<br><br>
		<input type="radio" name="gender" value="0">Male
		<br>
		<input type="radio" name="gender" value="1">Female
		<br>
		<input type="radio" name="gender" value="2">Other
		<br><br>

		<label>Nationality:</label>
		<select>
			<option value="indonesia">Indonesia</option>
			<option value="singapura">Singapura</option>
			<option value="malaysia">Malaysia</option>
			<option value="thailand">Thailand</option>
		</select>
		<br><br>

		<label>Language Spoken</label>
		<br><br>
		<input type="checkbox" name="language" value="0">Bahasa Indonesia
		<br>
		<input type="checkbox" name="language" value="1">English
		<br>
		<input type="checkbox" name="language" value="2">Arabic
		<br>
		<input type="checkbox" name="language" value="3">Japanese
		<br><br>

		<label for="bio">Bio:</label>
		<br>
		<textarea cols="30" rows="8" id="bio"></textarea>
		<br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>